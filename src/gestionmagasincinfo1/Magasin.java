/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionmagasincinfo1;

/**
 *
 * @author esprit
 */
public class Magasin {

    private int id;
    private String adresse;
    private String nom;
    private final int cap = 50;
    private int nbProduit = 0;

    private Produit tabP[];

    public Magasin(int id, String adresse) {
        this.id = id;
        this.adresse = adresse;
        this.tabP = new Produit[cap];

    }

    public Magasin(int id, String nom, String adresse) {
        this.id = id;
        this.adresse = adresse;
        this.tabP = new Produit[cap];
        this.nom = nom;

    }

    public String getAdresse() {
        return adresse;
    }

    public int getCap() {
        return cap;
    }

    public int getId() {
        return id;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void ajouterProduit(Produit p) {
        if (nbProduit < cap && cherhcerProduit(p) == false) {
            tabP[nbProduit] = p;
            nbProduit++;
        }
    }

    @Override
    public String toString() {

        String ch = id + " " + adresse + " " + cap;

        for (int i = 0; i < nbProduit; i++) {

            ch = ch + "\n" + tabP[i];

        }

        return ch; //To change body of generated methods, choose Tools | Templates.
    }

    public boolean cherhcerProduit(Produit p) {

        for (int i = 0; i < nbProduit; i++) {

            if (tabP[i].comparer(p)) {
                return true;
            }

        }
        return false;
    }

    public void supprimerProduit(Produit p) {
        int j;
        for (j = 0; j < nbProduit; j++) {
            if (tabP[j].comparer(p)) {
                tabP[j] = tabP[nbProduit - 1];
                tabP[nbProduit - 1] = null;
                nbProduit--;
                break;
            }
        }

    }

}

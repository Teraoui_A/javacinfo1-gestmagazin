/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionmagasincinfo1;

import java.util.Date;



/**
 *
 * @author esprit
 */
public class Produit {
    int id;
    String lib;
    String marque;
    float prix;
    Date dateExp;
    public static int NB_PRODUIT=0;

  public Produit(){
      NB_PRODUIT++;
  }
  
  public Produit(int id, String lib , String marque){
      this.id=id;
      this.lib=lib;
      this.marque=marque;
      NB_PRODUIT++;
      
              
      
  }

  
  public Produit(int id, String lib, String marque, float prix){
      
      this.id=id;
      this.lib=lib;
      this.marque=marque;
      this.prix=prix;
      NB_PRODUIT++;        
  }
  
  public void afficherProduit(){
      System.out.println(this.id + " "+ this.lib+ " "+this.marque+" "+this.prix);
  }

    @Override
    public String toString() {
        return id + " "+lib+" "+marque+ " "+prix;
                 //To change body of generated methods, choose Tools | Templates.
    }

    
  public boolean comparer(Produit p){
      
      return(p.id == this.id && this.lib.equals(p.lib) && this.prix == p.prix);
  }
  
  public static boolean comparer(Produit p1, Produit p2){
      
      return p1.id == p2.id && p1.lib.equals(p2.lib) && p1.prix==p2.prix;
  }
  
    
    
   
        
    
    
    
}
